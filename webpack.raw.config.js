// IMPORTANT!: 'webpack-stream' does not switch to React PROD mode
// use manual packing with the line:
//        webpack --config ./webpack.raw.config.js

const webpack = require('webpack');

var config = {
  entry: './app/scripts/react/main.js',
  output: {filename: './dist/scripts/min_index.js'},
  devtool: "nosources-source-map",
   module: {
      loaders: [
         {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
               presets: ['es2015', 'react']
            }
         }
      ]
   },
   plugins: [
     new webpack.DefinePlugin({
       'process.env': {
         NODE_ENV: JSON.stringify('production')
       }
     }),
     new webpack.optimize.UglifyJsPlugin()
  ]
}
module.exports = config;