# This is a test application demonstrating Google Map API, Golang request router and React.js frontend
 * user can create points of interest on map and save them in JSON format in local file
 * map supports two display modes: Normal and HeatMap

### Prerequisites
* [Download the SDK for App Engine](https://cloud.google.com/appengine/docs/standard/go/download)

### Building

```
gulp
```

### Running local server
```
dev_appserver.py dist
```

### Deployment
cd to directory contains 'app.yaml' file, then issue the following command
```
gcloud app deploy
```

### Accessing the application in Internet
Use url : [https://pathmap-123.appspot.com/](https://pathmap-123.appspot.com/)

### Useful info
* [setup IDE](https://medium.com/@daveym/setting-up-a-go-ide-osx-37adc75d40c4)


