package main

import (
	"fmt"
	"html/template"
	"io"
	"net/http"
	"time"
	//"appengine/datastore"
)

type Greeting struct {
	Author  string
	Content string
	Date    time.Time
}

var test = []Greeting{
	Greeting{
		"author0",
		"content0",
		time.Now(),
	},
	Greeting{
		"author1",
		"content1",
		time.Now(),
	},
	Greeting{
		"author1",
		"content1",
		time.Now(),
	},
	Greeting{
		"author2",
		"content2",
		time.Now(),
	}}

func root(w http.ResponseWriter, r *http.Request) {
	renderHtml("index.html", w, nil)
}

func renderHtml(fname string, w http.ResponseWriter, data interface{}) {
	renderer := template.Must(template.ParseFiles(fname))
	if err := renderer.Execute(w, data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func renderPlainText(w http.ResponseWriter, format string, a ...interface{}) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8") // normal header
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, fmt.Sprintf(format, a))
}

func init() {
	http.HandleFunc("/", root)
}
