// eslint-disable no-unused-vars 
// eslint browser: true
import React, {Component, PropTypes} from 'react';

const state = {IDLE: 0,
               ADDING_PATHS: 1,
               ITERRUPTED: 2,
               REACHED_LIMITS: 3,
               HEATMAP_SHOWN: 4,
               SKIP_MOUSECLICK_ONCE: 5};

export default class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      map: null,
      apikey: 'AIzaSyBkRHCvWSvHL0RDhayqX3jkcdGFAKCKGbg',
      objectsPerPath: 10,
      pathsLimit: 10,
      action: state.IDLE,
      lastCreatedPath: null,
      currObjectsPerPathCounter: 0,
      currPathConter: 0,
      // array for storing objects references for quick access
      paths: [],
      // Heatmap visualisation layer
      heatmap: null
    };
    // binded to Map context functions below that are executed in other context (callbacks)
    this.initMap = this.initMap.bind(this);
    this.onMouseClick = this.onMouseClick.bind(this);
    this.onNewPolyline = this.onNewPolyline.bind(this);

    // internal interface below
    this.clearPathCmd = this.clearPathCmd.bind(this);
    this.addPathCmd = this.addPathCmd.bind(this);
    this.getJsonCmd = this.getJsonCmd.bind(this);
    this.showHeatMap = this.showHeatMap.bind(this);
  }

  componentDidMount() {
    let tag = document.createElement('script');
    tag.async = true;
    tag.defer = true;
    // make the function accesible by Google API via alias
    window.initMap = this.initMap; /* eslint-disable no-undef */
    tag.src = `https://maps.googleapis.com/maps/api/js?key=${this.state.apikey}&callback=initMap&libraries=visualization`;
    document.body.appendChild(tag);
  }
  
  onNewPolyline(elem){
    if(this.state.action == state.HEATMAP_SHOWN){
      // prevent regenerated elements to be treated as new created
      return;
    }
    
    let geometry = elem.feature.getGeometry();
    let arr = geometry.getArray()
    if(arr.length == 1){
      // single point cannot form path, so exit the function
      return;
    }

    this.state.currPathConter++;
    if(this.state.currPathConter == this.state.pathsLimit){
      let toast = document.querySelector('#toast');
      toast.MaterialSnackbar.showSnackbar(
        {message: `You have reached limit of ${this.state.pathsLimit} paths`});
        this.setState({action: state.REACHED_LIMITS});
    }

    let item = {name: `Path${this.state.currPathConter}`,
                poly: elem,
                points: []};
    this.state.paths.push(item);
    let self = this;
    geometry.forEachLatLng(
      function(pos){
        var marker = new google.maps.Marker({
            position: pos,
            map: self.state.map
          });
        item.points.push(marker);
      });
    this.props.notifyUpdate(this.state.paths);
    if(this.state.action == state.ITERRUPTED){
      if(this.state.currPathConter == this.state.pathsLimit){
        this.setState({action: state.REACHED_LIMITS});
      }else{
        this._setIdleState();
      }
    }else{
      this.setState({action: state.SKIP_MOUSECLICK_ONCE});
    }
  }

  onMouseClick(e){
    if(this.state.action == state.HEATMAP_SHOWN ||
        this.state.action == state.REACHED_LIMITS){
      return;
    }
    
    if(this.state.action == state.SKIP_MOUSECLICK_ONCE){
      if(this.state.currPathConter == this.state.pathsLimit){
        this.setState({action: state.REACHED_LIMITS});
      }else{
        this._setIdleState();
      }
      return;
    }
    
    this.setState({action: state.ADDING_PATHS});
    this.state.currObjectsPerPathCounter++;
    if(this.state.currObjectsPerPathCounter == this.state.objectsPerPath){
      this._setInterruptedState();
    }
  }

  _setIdleState(){
    this.setState({action: state.IDLE});
    this.state.currObjectsPerPathCounter = 0;
    this.state.map.data.setDrawingMode('LineString');
  }

  _setInterruptedState(){
    this.setState({action: state.ITERRUPTED});
    this.state.map.data.setDrawingMode(null);
  }

  clearPathCmd(){
    if(this.state.action == state.HEATMAP_SHOWN){
      // prevent regenerated elements to be treated as new created
      return;
    }

    if(this.state.action == state.ADDING_PATHS){
      this.state.map.data.setDrawingMode(null);
    }
    let item = this.state.paths.pop();
    if(!!item){
      item.points.forEach((e) => {e.setMap(null);});
      this.state.map.data.remove(item.poly.feature);
      this.state.currPathConter--;
      this.props.notifyUpdate(this.state.paths);
    }
    this._setIdleState();
  }
  
  addPathCmd(){
    if(this.state.action == state.ADDING_PATHS){
      this._setInterruptedState();
    }
  }
  
  getJsonCmd(){
    if(this.state.paths.length == 0){
      return null;
    }
    let result = this.state.paths.map((e)=>{
      let points = e.points.map((p) =>{
        let pos = p.getPosition(); 
        return {pos: {lat: pos.lat(), lng: pos.lng()}} });
      return {name: e.name, points: points}});
    return result;
  }
  
  showHeatMap(){
    if(this.state.paths.length == 0 || 
        this.state.action == state.ADDING_PATHS){
      return;
    }
    if(this.state.action == state.HEATMAP_SHOWN){
      this.state.heatmap.setMap(null);
      this.state.paths.forEach((e)=>{
        this.state.map.data.add(e.poly.feature);
        e.points.forEach((p) =>{p.setMap(this.state.map)});
      })
      
      if(this.state.currPathConter == this.state.pathsLimit){
        this.setState({action: state.REACHED_LIMITS});
      }else{
        this._setIdleState();
      }
    }else{
      this.state.paths.forEach((e)=>{
        this.state.map.data.remove(e.poly.feature);
        e.points.forEach((p) =>{p.setMap(null)});
      })
      let arr = this.state.paths.map((e)=>{return e.points.map((p) =>{ return p.getPosition()})});
      let heatmapData = arr.reduce((e,a) =>{return a.concat(e)}, []);
      let heatmap = new google.maps.visualization.HeatmapLayer({
        data: heatmapData
      });
      this.setState({heatmap: heatmap});
      heatmap.setMap(this.state.map);
      this.state.map.data.setDrawingMode(null);
      this.setState({action: state.HEATMAP_SHOWN});
    }
  }
  
  initMap() {
    let mapDiv = document.getElementById('map');
    let map = new google.maps.Map(mapDiv, // eslint-disable-line no-undef
        {
          zoom: 14,
          center: {lat: 60.1675, lng: 24.9311},
          // see more:
          // https://developers.google.com/maps/documentation/javascript/reference#Map
          mapTypeControl: false,
          zoomControl: false,
          scaleControl: false,
          streetViewControl: false,
          fullscreenControl: false,
          navigationControl: false,
          draggable: false,
          scrollwheel: false,
          disableDoubleClickZoom: true
        });
    map.data.setDrawingMode('LineString');
    google.maps.event.addDomListener(mapDiv, 'click', this.onMouseClick);
    map.data.addListener('addfeature', this.onNewPolyline);
    this.setState({map: map});
  }

  render() {
    return (
        <div className='mdl-cell mdl-cell--12-col' id='mapcontainer'>
          <div className='mdl-grid'>
            <div className='mdl-layout-spacer'></div>
              <div id='map'></div>
              <div id='toast' className='mdl-js-snackbar mdl-snackbar'>
                <div className='mdl-snackbar__text'></div>
                <button className='mdl-snackbar__action' type='button'></button>
              </div>
            <div className='mdl-layout-spacer'></div>
          </div>
        </div>
      );
  }
}
