import React, { Component, PropTypes } from 'react'
import Path from './Path.js'

export default class PathList extends React.Component {
  render() {
    return (
        <div className="mdl-grid">
          <div className="mdl-layout-spacer"></div>
          <div className="scollable-list mdl-layout__content">
                <ul className="mdl-list">
                  {!this.props.model ? <Path/> : this.props.model.map((item, i) => {return(<Path key={i} descr={item.name}/>)})}
                </ul>
          </div>
          <div className="mdl-layout-spacer"></div>
        </div>
    );
  }
}
