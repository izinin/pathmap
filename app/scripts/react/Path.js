import React, { Component, PropTypes } from 'react'

export default class Path extends React.Component {  
  render() {
    let name = !this.props.descr ? "Nothing defined" : this.props.descr;
    return (
      <li className="mdl-list__item">
        <span className="mdl-list__item-primary-content">
          { name }
        </span>
      </li>
    );
  }
}
