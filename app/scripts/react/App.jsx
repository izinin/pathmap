import React from 'react';
import Map from './Map.js'
import PathList from './PathList.js'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      map: null  // reference to Google map instance
    }
    this.onMapDataChanged = this.onMapDataChanged.bind(this);
    this.clearPathBtn = this.clearPathBtn.bind(this);
    this.savePathBtn = this.savePathBtn.bind(this);
    this.heatMapBtn = this.heatMapBtn.bind(this);
    this.exportJSON = this.exportJSON.bind(this);
  }
  
  onMapDataChanged(data){
    this.setState({map: data});
  }

  clearPathBtn(){
    this.mapInstance.clearPathCmd();
  }

  savePathBtn(){
    this.mapInstance.addPathCmd();
  }

  heatMapBtn(){
    this.mapInstance.showHeatMap();
  }

  exportJSON(){
    if(!this.mapInstance){
      return "";
    }
    let obj = this.mapInstance.getJsonCmd();
    if(!obj){
      return "";
    }
    
    return btoa(JSON.stringify(obj, null, 2));
  }
  
  render() {
    return (
   <div className="mdl-layout">  
     <div className="mdl-grid">
       
       <Map notifyUpdate = {this.onMapDataChanged} ref={((instance) => {this.mapInstance = instance;})}/>
       
       <div className="mdl-cell mdl-cell--12-col">
         <div className="mdl-grid">
           <div className="mdl-layout-spacer"></div>
           <div className="pathmap-button-row">
             <button id="clearpath" className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" onClick={this.clearPathBtn}>
               Clear path
             </button>
           </div>
           <div className="pathmap-button-row">
             <button id="savepath" className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" onClick={this.savePathBtn}>
               Save Path
             </button>
           </div>
           <div className="pathmap-button-row">
             <button id="show-heatmap" className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" onClick={this.heatMapBtn}>
               Show heat map
             </button>
           </div>
           <div className="mdl-layout-spacer"></div>
         </div>
       </div>
       
       <div className="mdl-cell mdl-cell--12-col">
         <div className="mdl-grid">
           <div className="mdl-layout-spacer"></div>
           <h4>Saved path</h4>
           <div className="mdl-layout-spacer"></div>
         </div>
       </div>
         
      <PathList model = { this.state.map }/>
       
       <div className="mdl-cell mdl-cell--12-col">
         <div className="mdl-grid">
           <div className="mdl-layout-spacer"></div>
           <div className="pathmap-button-row">
             <a id="download-link" href={"data:;base64," + this.exportJSON()} download="paths.json" 
                onClick={(!!this.state.map && this.state.map.length > 0) ? null : (e) => {e.preventDefault()}}>
               <button id="export-json" 
                  className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" >
                 Export as JSON
               </button>
             </a>
           </div>
           <div className="mdl-layout-spacer"></div>
         </div>
       </div>

     </div>
   </div>  
    );
  }
}

export default App;

